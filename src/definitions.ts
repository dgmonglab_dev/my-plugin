declare module '@capacitor/core' {
  interface PluginRegistry {
    MyPlugin: MyPluginPlugin;
  }
}
//kakaoLogin
export interface MyPluginPlugin {
    echo(options: { value: string }): Promise<{ value: string }>;
    kakaoLogin(options: { value: string }): Promise<{ value: string }>;
}
