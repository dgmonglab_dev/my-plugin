import Foundation
import Capacitor
import KakaoSDKCommon
import KakaoSDKAuth
import KakaoSDKUser

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(MyPlugin)
public class MyPlugin: CAPPlugin {

    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.success([
            "value": value
        ])
    }
    
    @objc func kakaoLogin(_ call: CAPPluginCall) {
        print("MyPlugin.kakaoLogin")
        if (AuthApi.isKakaoTalkLoginAvailable()) {
            AuthApi.shared.loginWithKakaoTalk {(oauthToken, error) in
                if let error = error {
                    print(error)
                    call.error("hasError")
                }
                else {
                    print("loginWithKakaoTalk() success.")

                    //do something
                    let SDKtoken = oauthToken
                    let asscessToken = SDKtoken?.accessToken;
                    call.success([
                        "token":asscessToken
                    ])
                }
            }
        }else{

            AuthApi.shared.loginWithKakaoAccount {(oauthToken, error) in
                        if let error = error {
                            print(error)
            //                 call.success([
            //                     "error":error
            //                 ])
                            call.error("hasError")
                        }
                        else {
                            print("loginWithKakaoAccount() success.")

                            //do something
                            let SDKtoken = oauthToken
                            let asscessToken = SDKtoken?.accessToken;
                            call.success([
                                "token":asscessToken
                            ])
                        }
                    }
        }
        //KakaoSDKCommon.initSDK(appKey: "7d7994c75553afd27ae455e80f797be7", loggingEnable:true);
        
    }
}
